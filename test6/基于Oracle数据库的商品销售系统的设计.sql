--创建产品表
CREATE TABLE product_info (
  product_id NUMBER(9, 0) PRIMARY KEY,
  product_name VARCHAR2(100),
  description VARCHAR2(200),
  price NUMBER(8, 2)
);
CREATE TABLE inventory (
  inventory_id NUMBER(9, 0) PRIMARY KEY,
  product_id NUMBER(9, 0),
  quantity NUMBER(10, 0),
  location VARCHAR2(100),
  CONSTRAINT inventory_fk1 FOREIGN KEY (product_id) REFERENCES product_info (product_id)
);


--插入函数
CREATE OR REPLACE PROCEDURE generate_fake_data AS
  v_num_rows NUMBER := 100000;
BEGIN
  FOR i IN 1..v_num_rows LOOP
    -- 插入产品信息
    INSERT INTO product_info (product_id, product_name, description, price)
    VALUES (i, 'Product ' || i, 'Description ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
    
    -- 插入库存信息
    INSERT INTO inventory (inventory_id, product_id, quantity, location)
    VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 100), 0), 'Location ' || i);
  END LOOP;
  
  COMMIT;
  DBMS_OUTPUT.PUT_LINE(v_num_rows || ' rows of fake data inserted into product_info and inventory tables.');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    ROLLBACK;
END;
/


-- 执行插入函数，插入十万条
BEGIN
  generate_fake_data;
END;


--创建用户表
CREATE TABLE customer_info (
  customer_id NUMBER(9, 0) PRIMARY KEY,
  customer_name VARCHAR2(100),
  contact_info VARCHAR2(100)
);
CREATE TABLE user_info (
  user_id NUMBER(9, 0) PRIMARY KEY,
  username VARCHAR2(100),
  password VARCHAR2(100),
  role VARCHAR2(50),
  customer_id NUMBER(9, 0),
  CONSTRAINT user_info_fk1 FOREIGN KEY (customer_id) REFERENCES customer_info (customer_id)
);



--创建角色
CREATE ROLE admin_role;
CREATE ROLE user_role;

CREATE USER admin_user IDENTIFIED BY 123;
CREATE USER regular_user IDENTIFIED BY 123;

GRANT admin_role TO admin_user;
GRANT user_role TO regular_user;


-- 给管理员角色授予对所有表的完全访问权限
GRANT ALL PRIVILEGES TO admin_role;

-- 给普通用户角色授予对指定表的只读访问权限
GRANT SELECT ON product_info TO user_role;
GRANT SELECT ON inventory TO user_role;


-- 给管理员用户授予创建表的权限
GRANT CREATE TABLE TO admin_user;


CREATE OR REPLACE FUNCTION insert_order_data
RETURN NUMBER
IS
   v_customer_id   NUMBER;
   v_product_id    NUMBER;
   v_order_date    DATE;
   v_quantity      NUMBER;
   v_order_id      NUMBER;
BEGIN
   FOR i IN 1..100 LOOP
      -- 随机选择客户ID
      SELECT customer_id
      INTO v_customer_id
      FROM customer_info
      ORDER BY DBMS_RANDOM.VALUE
      FETCH FIRST 1 ROWS ONLY;

      -- 随机选择产品ID
      SELECT product_id
      INTO v_product_id
      FROM product_info
      ORDER BY DBMS_RANDOM.VALUE
      FETCH FIRST 1 ROWS ONLY;
      
      -- 生成订单日期
      v_order_date := SYSDATE - DBMS_RANDOM.VALUE(1, 30);
      
      -- 生成订单数量
      v_quantity := ROUND(DBMS_RANDOM.VALUE(1, 10));
      
      -- 插入订单数据
      INSERT INTO orders_info (order_id, customer_id, product_id, order_date, quantity)
      VALUES (SEQ1.nextval, v_customer_id, v_product_id, v_order_date, v_quantity);
   END LOOP;
   
   COMMIT;
   
   RETURN 1;
END;
/


BEGIN
  INSERT_ORDER_DATA;
END;
/


--创建订单表
CREATE TABLE orders_info (
   order_id     NUMBER,
   customer_id  NUMBER,
   product_id   NUMBER,
   order_date   DATE,
   quantity     NUMBER,
   CONSTRAINT orders_pk_1 PRIMARY KEY (order_id),
   CONSTRAINT orders_fk_customer_1 FOREIGN KEY (customer_id) REFERENCES customer_info (customer_id),
   CONSTRAINT orders_fk_product_1 FOREIGN KEY (product_id) REFERENCES product_info (product_id)
);


--简单查询语句
SELECT ci.CUSTOMER_ID, oi.ORDER_ID, oi.ORDER_DATE, oi.PRODUCT_ID, oi.QUANTITY, pi.PRODUCT_NAME, pi.PRICE, pi.DESCRIPTION
FROM CUSTOMER_INFO ci
LEFT JOIN ORDERS_INFO oi ON ci.CUSTOMER_ID = oi.CUSTOMER_ID
LEFT JOIN PRODUCT_INFO pi ON oi.PRODUCT_ID = pi.PRODUCT_ID
WHERE ci.CUSTOMER_ID < 100
ORDER BY ci.CUSTOMER_ID;

--创建索引
CREATE INDEX idx_customer ON ORDERS_INFO(CUSTOMER_ID)


