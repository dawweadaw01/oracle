# 实验6（期末考核） 基于Oracle数据库的商品销售系统的设计

[TOC]



### 1.产品管理设计

​	产品信息表：存储产品的基本信息，如产品ID、名称、描述、价格等。

​	库存表：记录每个产品的库存数量和位置信息。

#### 创建这两个表的创建语句

​	

```sql
CREATE TABLE product_info (
  product_id NUMBER(9, 0) PRIMARY KEY,
  product_name VARCHAR2(100),
  description VARCHAR2(200),
  price NUMBER(8, 2)
);
CREATE TABLE inventory (
  inventory_id NUMBER(9, 0) PRIMARY KEY,
  product_id NUMBER(9, 0),
  quantity NUMBER(10, 0),
  location VARCHAR2(100),
  CONSTRAINT inventory_fk1 FOREIGN KEY (product_id) REFERENCES product_info (product_id)
);
```

![image-20230516153503489](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516153503489.png)

#### 创建出了一个函数，设计了10万条假数据

```sql
CREATE OR REPLACE PROCEDURE generate_fake_data AS
  v_num_rows NUMBER := 100000;
BEGIN
  FOR i IN 1..v_num_rows LOOP
    -- 插入产品信息
    INSERT INTO product_info (product_id, product_name, description, price)
    VALUES (i, 'Product ' || i, 'Description ' || i, ROUND(DBMS_RANDOM.VALUE(10, 1000), 2));
    
    -- 插入库存信息
    INSERT INTO inventory (inventory_id, product_id, quantity, location)
    VALUES (i, i, ROUND(DBMS_RANDOM.VALUE(1, 100), 0), 'Location ' || i);
  END LOOP;
  
  COMMIT;
  DBMS_OUTPUT.PUT_LINE(v_num_rows || ' rows of fake data inserted into product_info and inventory tables.');
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Error: ' || SQLERRM);
    ROLLBACK;
END;
/
```

![image-20230516153540452](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516153540452.png)

#### 执行

```sql
-- 执行插入函数，插入十万条
BEGIN
  generate_fake_data;
END;
```

<img src="https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516153645453.png" alt="image-20230516153645453" style="zoom:67%;" />

到此，商品数据创建完毕

### 2.客户管理

客户信息表：保存客户的基本信息，如客户ID、姓名、联系方式等 

用户表：存储系统用户的信息，如用户名、密码、角色等。

#### 建表语句

```sql
CREATE TABLE customer_info (
  customer_id NUMBER(9, 0) PRIMARY KEY,
  customer_name VARCHAR2(100),
  contact_info VARCHAR2(100)
);
CREATE TABLE user_info (
  user_id NUMBER(9, 0) PRIMARY KEY,
  username VARCHAR2(100),
  password VARCHAR2(100),
  role VARCHAR2(50),
  customer_id NUMBER(9, 0),
  CONSTRAINT user_info_fk1 FOREIGN KEY (customer_id) REFERENCES customer_info (customer_id)
);
```

![image-20230516163022963](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516163022963.png)

使用navicat创建数据



#### 创建插入函数

插入100条数据

![image-20230516163339302](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516163339302.png)



### 3.订单管理

创建订单表

```sql
CREATE TABLE orders_info (
   order_id     NUMBER,
   customer_id  NUMBER,
   product_id   NUMBER,
   order_date   DATE,
   quantity     NUMBER,
   CONSTRAINT orders_pk_1 PRIMARY KEY (order_id),
   CONSTRAINT orders_fk_customer_1 FOREIGN KEY (customer_id) REFERENCES customer_info (customer_id),
   CONSTRAINT orders_fk_product_1 FOREIGN KEY (product_id) REFERENCES product_info (product_id)
);
```

![image-20230516172659836](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516172659836.png)



### 结果验证

#### 简单查询

```sql
SELECT ci.CUSTOMER_ID, oi.ORDER_ID, oi.ORDER_DATE, oi.PRODUCT_ID, oi.QUANTITY, pi.PRODUCT_NAME, pi.PRICE, pi.DESCRIPTION
FROM CUSTOMER_INFO ci
LEFT JOIN ORDERS_INFO oi ON ci.CUSTOMER_ID = oi.CUSTOMER_ID
LEFT JOIN PRODUCT_INFO pi ON oi.PRODUCT_ID = pi.PRODUCT_ID
WHERE ci.CUSTOMER_ID = 1;
```

![image-20230516173635770](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516173635770.png)

![image-20230516173704943](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516173704943.png)

```sql
# 查询id小于的customer的购买记录
SELECT ci.CUSTOMER_ID, oi.ORDER_ID, oi.ORDER_DATE, oi.PRODUCT_ID, oi.QUANTITY, pi.PRODUCT_NAME, pi.PRICE, pi.DESCRIPTION
FROM CUSTOMER_INFO ci
LEFT JOIN ORDERS_INFO oi ON ci.CUSTOMER_ID = oi.CUSTOMER_ID
LEFT JOIN PRODUCT_INFO pi ON oi.PRODUCT_ID = pi.PRODUCT_ID
WHERE ci.CUSTOMER_ID < 100
ORDER BY ci.CUSTOMER_ID;
```

![image-20230516174029395](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516174029395.png)

#### 给订单表添加索引，加快查询速度

##### 添加前

![image-20230516175219827](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516175219827.png)

![image-20230516175720743](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516175720743.png)

##### 添加后

可以看出少扫描了一百行

![image-20230516175931462](D:\Oracle作业\gitlab\oracle\test6\img\image-20230516175931462.png)

### 创建角色

```sql
CREATE ROLE admin_role;
CREATE ROLE user_role;
```



![image-20230516164957802](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516164957802.png)



#### 创建用户

##### 	创建用户并分配角色。每个用户将被分配一个或多个角色。

```sql
CREATE USER admin_user IDENTIFIED BY password;
CREATE USER regular_user IDENTIFIED BY password;

GRANT admin_role TO admin_user;
GRANT user_role TO regular_user;
```



![image-20230516165408057](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516165408057.png)

##### 赋予权限

```sql
-- 给管理员角色授予对所有表的完全访问权限
GRANT ALL PRIVILEGES TO admin_role;

-- 给普通用户角色授予对指定表的只读访问权限
GRANT SELECT ON product_info TO user_role;
GRANT SELECT ON inventory TO user_role;
```

![image-20230516165514261](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516165514261.png)

从下图可以看出admin的角色权限，以及它的成员

![image-20230516170354692](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516170354692.png)

这是regular_user的权限

![image-20230516170658026](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516170658026.png)



### 4.数据库备份

1. 使用Oracle Recovery Manager (RMAN)：RMAN是Oracle官方提供的备份和恢复工具，具有强大的功能和灵活性。使用RMAN可以执行完全备份、增量备份、差异备份以及增量合并备份等操作。备份可以存储在磁盘、磁带或云存储中。
2. 使用Data Pump工具：Oracle Data Pump是一种实用工具，可用于导出和导入数据库对象和数据。通过使用Data Pump的导出功能，可以将整个数据库或特定表空间、用户或表导出为可恢复的备份文件。导出的文件可以存储在磁盘或其他媒体中。
3. 使用物理备份：物理备份是将数据库文件直接复制到备份目标位置的一种方法。可以使用文件系统工具（如cp、rsync）或存储快照技术来执行物理备份。
4. 使用逻辑备份：逻辑备份是将数据库逻辑对象（如表、视图、存储过程）的逻辑定义和数据导出到备份文件中的一种方法。可以使用工具如expdp（导出数据泵）或第三方工具（如Toad、PL/SQL Developer）来执行逻辑备份。

​	这里实现简单的备份，数据导出方式备份

![image-20230516181052642](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516181052642.png)

![image-20230516181220639](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test6/img/image-20230516181220639.png)
