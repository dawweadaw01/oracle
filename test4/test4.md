# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriangle，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriangle的SQL语句。

## 创建杨辉三角储存函数的源代码

```sql
CREATE OR REPLACE PROCEDURE YHTriangle(N IN INTEGER) AS
    type t_number is varray (100) of integer not null;
    i integer;
    j integer;
    spaces varchar2(30) :='   ';
    rowArray t_number := t_number();
BEGIN
    dbms_output.put_line('1');
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put(rpad(1,9,' '));
    dbms_output.put_line('');
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N loop
        rowArray(i):=1;
        j:=i-1;
        while j>1 loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
        for j in 1 .. i loop
            dbms_output.put(rpad(rowArray(j),9,' '));
        end loop;
        dbms_output.put_line('');
    end loop;
END;
/
```

![image-20230515104206789](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test4/img/image-20230515104206789.png)

## 调用储存函数

```sql
EXEC YHTriangle(N);
```

![image-20230515104624387](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test4/img/image-20230515104624387.png)

