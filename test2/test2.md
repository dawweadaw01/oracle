# 实验2：用户及权限管理

姓名:刘豪杰  	学号：202010414209  	班级；软件工程2班

## 实验目的

掌握用户管理、角色管理、权根维护与分配的能力，掌握用户之间共享对象的操作技能，以及概要文件对用户的限制。

## 实验内容

Oracle有一个开发者角色resource，可以创建表、过程、触发器等对象，但是不能创建视图。本训练要求：

- 在pdborcl插接式数据中创建一个新的本地角色con_res_role，该角色包含connect和resource角色，同时也包含CREATE VIEW权限，这样任何拥有con_res_role的用户就同时拥有这三种权限。
- 创建角色之后，再创建用户sale，给用户分配表空间，设置限额为50M，授予con_res_role角色。
- 最后测试：用新用户sale连接数据库、创建表，插入数据，创建视图，查询表和视图的数据。

## 实验参考步骤

对于以下的对象名称con_res_role，sale，在实验的时候应该修改为自己的名称。

- 第1步：以system登录到pdborcl，创建角色con_res_role和用户sale，并授权和分配空间：
- ![image-20230424142100951](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/%7B$Z$YW9%5D7IIH6UR3X%606N%25U8.png)

> 语句“ALTER USER sale QUOTA 50M ON users;”是指授权sale用户访问users表空间，空间限额是50M。

- 第2步：新用户sale连接到pdborcl，创建表customers和视图customers_view，插入数据，最后将customers_view的SELECT对象权限授予hr用户。session_privs，session_roles可以查看会话权限和角色。
- ![image-20230424142330026](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424142622797.png)

- 第3步：用户hr连接到pdborcl，查询sale授予它的视图customers_view

```sql
$ sqlplus hr/123@pdborcl
SELECT * FROM sale.customers;             
SELECT * FROM sale.customers_view;
```

![image-20230424142622797](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424142622797.png)

> 测试一下用户hr,sale之间的表的共享，只读共享和读写共享都测试一下。
> sale用户只共享了视图customers_view给hr,没有共享表customers。所以hr无法查询表customers。

## 概要文件设置,用户最多登录时最多只能错误3次

```sql
$ sqlplus system/123@pdborcl
SQL>ALTER PROFILE default LIMIT FAILED_LOGIN_ATTEMPTS 3;
```

![image-20230424143118337](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424143118337.png)

![image-20230424143359743](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424143359743.png)

- 设置后，sqlplus hr/错误密码@pdborcl 。3次错误密码后，用户被锁定。
- 锁定后，通过system用户登录，alter user sale unlock命令解锁。

```sh
$ sqlplus system/123@pdborcl;
alter user sale  account unlock;
```

## 数据库和表空间占用分析

> 当实验做完之后，数据库pdborcl中包含了新的角色con_res_role和用户sale。
> 新用户sale使用默认表空间users存储表的数据。
> 随着用户往表中插入数据，表空间的磁盘使用量会增加。

## 查看数据库的使用情况

```sql
$ sqlplus system/123@pdborcl

SELECT tablespace_name,FILE_NAME,BYTES/1024/1024 MB,MAXBYTES/1024/1024 MAX_MB,autoextensible FROM dba_data_files  WHERE  tablespace_name='USERS';

SELECT a.tablespace_name "表空间名",Total/1024/1024 "大小MB",
 free/1024/1024 "剩余MB",( total - free )/1024/1024 "使用MB",
 Round(( total - free )/ total,4)* 100 "使用率%"
 from (SELECT tablespace_name,Sum(bytes)free
        FROM   dba_free_space group  BY tablespace_name)a,
       (SELECT tablespace_name,Sum(bytes)total FROM dba_data_files
        group  BY tablespace_name)b
 where  a.tablespace_name = b.tablespace_name;
```

- autoextensible是显示表空间中的数据文件是否自动增加。

- MAX_MB是指数据文件的最大容量。

  ![image-20230424143701131](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424143701131.png)

## 实验结束删除用户和角色

```sh
$ sqlplus system/123@pdborcl
drop role con_res_role;
drop user sale cascade;
```

![image-20230424143750840](https://gitlab.com/dawweadaw01/oracle/-/raw/main/test2/img/image-20230424143750840.png)

