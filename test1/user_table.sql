/*
 Navicat Premium Data Transfer

 Source Server         : mysql8
 Source Server Type    : MySQL
 Source Server Version : 80030 (8.0.30)
 Source Host           : localhost:3306
 Source Schema         : atguigudb1

 Target Server Type    : MySQL
 Target Server Version : 80030 (8.0.30)
 File Encoding         : 65001

 Date: 24/04/2023 13:04:56
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for user_table
-- ----------------------------
DROP TABLE IF EXISTS `user_table`;
CREATE TABLE `user_table`  (
  `user_id` int NOT NULL,
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `gender` char(1) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL,
  `age` int NOT NULL,
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_table
-- ----------------------------
INSERT INTO `user_table` VALUES (1, '张三', '男', 25, '2022-04-18 10:00:00');
INSERT INTO `user_table` VALUES (2, '李四', '女', 30, '2022-04-18 11:00:00');
INSERT INTO `user_table` VALUES (3, '王五', '男', 28, '2022-04-18 12:00:00');
INSERT INTO `user_table` VALUES (4, '赵六', '女', 27, '2022-04-18 13:00:00');
INSERT INTO buy_table (buy_id, user_id, product_name, price, buy_time) VALUES
(1, 1, '商品A', 100.00, '2022-04-18 14:00:00'),
(2, 1, '商品B', 200.00, '2022-04-18 15:00:00'),
(3, 2, '商品C', 300.00, '2022-04-18 16:00:00'),
(4, 2, '商品D', 400.00, '2022-04-18 17:00:00'),
(5, 2, '商品E', 500.00, '2022-04-18 18:00:00'),
(6, 3, '商品F', 600.00, '2022-04-18 19:00:00'),
(7, 3, '商品G', 700.00, '2022-04-18 20:00:00'),
(8, 4, '商品H', 800.00, '2022-04-18 21:00:00'),
(9, 4, '商品I', 900.00, '2022-04-18 22:00:00');


SET FOREIGN_KEY_CHECKS = 1;
